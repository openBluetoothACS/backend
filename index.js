let mysql = require('mysql');
let dateFormat = require('dateformat');
let config = require('./libs/config');
let express = require('express');
let app = express();
let server = require('http').Server(app);
let io = require('socket.io')(server);
let fn = require('./libs/fn'); //функции
let sockets = require('./libs/socket-path');

server.listen(config.get('port'), function () {
    console.log('listening on *:' + config.get('port'));
});

app.use(express.static(__dirname + '/public'));

// io.on('connection', sockets.paths(socket));
//
io.on('connection', function (socket) {
    console.log('a user connected');
    // fn.getStatisticsOfAnswers(socket);
    fn.getListLocations(socket);
    socket.on('updateScanner', function (dataset) {
        fn.updateScanner(dataset,socket);
    });
    socket.on('getScanners', function () {
        fn.getScanners(socket);
    });
    socket.on('additem', function (dataset) {
        fn.addItem(dataset, socket);
    });
    socket.on('getRounds', function (dataset) {
        fn.getRounds(dataset,socket);
    });
    socket.on('getCandidates', function () {
        fn.getCandidates(socket);
    });
    socket.on('getRoundsForMachine', function (dataset) {
        fn.getRoundsForMachine(dataset,socket);
    });
    socket.on('getLocationsList', function () {
        fn.getListLocations(socket);
    })
    socket.on('lastseen', function () {
        fn.lastSeen(socket);
    })
    socket.on('lastseenScanner', function () {
        fn.lastSeenScanners(socket);
    })
    socket.on('regions', function () {
        fn.getRegions(socket);
    })
    socket.on('areas', function (dataset) {
        fn.getAreas(dataset, socket);
    })
    socket.on('locations', function (dataset) {
        fn.getLocations(dataset, socket);
    })
    socket.on('addnewscanner', function (dataset) {
        fn.addNewScanner(dataset, socket);
    })
});