let mysql = require('mysql');
let dateformat = require('dateformat');
let config = require('./config');

let connection = mysql.createConnection(config.get('mysql'));

module.exports.lastSeen = function (socket) {
    let sql = "SELECT * FROM machines\n" +
        "LEFT JOIN (\n" +
        " SELECT mac, raspberryId, MAX(end) AS lastSeen\n" +
        "  FROM rounds\n" +
        "  GROUP BY mac\n" +
        ") temptable\n" +
        "ON temptable.mac = machines.mac\n" +
        "\n" +
        "LEFT JOIN (\n" +
        "SELECT mac as rbp_mac, raspberryId as rbp_id FROM rounds \n" +
        "WHERE end in \n" +
        "(SELECT MAX(end) AS lastSeen\n" +
        "  FROM rounds\n" +
        "  GROUP BY mac)\n" +
        ") rbpid\n" +
        "ON rbpid.rbp_mac = temptable.mac\n" +
        "\n" +
        "LEFT JOIN (\n" +
        " SELECT name as location_name, id\n" +
        "  FROM raspberries\n" +
        "  \n" +
        ") temptablerspb\n" +
        "ON rbpid.rbp_id = temptablerspb.id";
    connection.query(sql, function (err, results, fields) {
        console.log('lastseen');
        socket.emit('lastseen', results);
    })
}

module.exports.lastSeenScanners = function (socket) {
    let sql = "SELECT * FROM raspberries\n" +
        "LEFT JOIN (\n" +
        "SELECT raspberryId, MAX(end) AS lastSeen\n" +
        "FROM rounds\n" +
        "GROUP BY raspberryId\n" +
        ")temptable\n" +
        "ON temptable.raspberryId = raspberries.id";
    connection.query(sql, function (err, results, fields) {
        console.log('lastSeenScanners')
        socket.emit('lastseenScanners', results)
    })
}

module.exports.getRoundsBad = function (dataset, socket) {
    if ((typeof dataset.date === 'undefined') || (typeof dataset.shift === 'undefined') || (typeof dataset.locationId === 'undefined')) {
        return;
    }
    let sql =
        "SELECT * FROM raspberries " +
        "WHERE raspberries.locationId = " + dataset.locationId.toString().replace(/\D/g) + "\n";
    connection.query(sql, (err, results, fields) => {
        for (let i in results) {
            console.log(results[i].id);
            // dataset.raspberryId = results[i].id;
            getRoundsFor(dataset, socket, results[i]);
        }
    })
};

module.exports.getRounds = (dataset, socket, raspberry) => {
    if ((typeof dataset.date === 'undefined') || (typeof dataset.shift === 'undefined') || (typeof dataset.locationId === 'undefined')) {
        return;
    }
    let timeStart = "";
    let timeEnd = "";
    let start = "";
    let end = "";
    switch (dataset.shift) {
        case 'day':
            start = new Date(dataset.date + ' 01:00:00.000Z');
            end = new Date(dataset.date + ' 13:00:00.000Z');
            break;
        case 'night':
            start = new Date(dataset.date + ' 13:00:00.000Z');
            console.log(start);
            end = new Date(dataset.date + ' 13:00:00.000Z');
            console.log(end);
            end.setHours(end.getHours() + 12);
            console.log(end);
            break;
        default:
            start = new Date(dataset.date + ' 01:00:00.000Z');
            end = new Date(dataset.date + ' 01:00:00.000Z');
            end.setHours(end.getHours() + 24);
            break;
    }

    console.log('fullState');

    let sql = "SELECT " +
        "rounds.id\n" +
        ", rounds.mac\n" +
        ", COUNT(rounds.mac) as count\n" +
        ", machines.name\n" +
        ", machines.number\n" +
        ", locations.name as location\n" +
        ", locations.id as locationId\n" +
        ", '" + dataset.date + "' as date\n" +
        ", '" + dataset.shift + "' as shift\n" +
        "FROM truks.rounds\n" +
        "LEFT JOIN truks.machines\n" +
        "ON (truks.machines.mac = truks.rounds.mac)\n" +
        "LEFT JOIN truks.raspberries\n" +
        "ON (truks.raspberries.id = truks.rounds.raspberryId)" +
        "LEFT JOIN truks.locations\n" +
        "ON (truks.locations.id = " + dataset.locationId.toString().replace(/\D/g) + ")\n" +
        "WHERE \n" +
        "rounds.mac in (\n" +
        "SELECT DISTINCT mac FROM machines\n" +
        ")\n" +
        "AND rounds.raspberryId in (\n" +
        "\tSELECT id FROM raspberries WHERE locationId = " + dataset.locationId.toString().replace(/\D/g)
        + "\n" +
        ")" +

        "AND start > \"" + dateformat(start, config.get('dateformat')) + "\" \n" +
        "AND end < \"" + dateformat(end, config.get('dateformat')) + "\" \n" +
        "AND TIME_TO_SEC(end) - TIME_TO_SEC(start) > timeout" + "\n" +
        "GROUP BY rounds.mac";


    console.log('sql: ', sql);

    connection.query(sql, function (error, results, fields) {
        // console.log(results);
        socket.emit('fullState', results);

    })

};

module.exports.updateScanner = (dataset, socket) => {
    console.log(dataset);
    dataset.sql = "UPDATE `truks`.`raspberries`\n" +
        "SET\n" +
        "`name` = '" + dataset.name + "',\n" +
        "`timeout` = " + dataset.timeout.toString().replace(/\D/g, "") + "\n" +
        "WHERE `id` = " + dataset.id;
    dataset.emit = "updateScannerSucessfull";
    console.log(dataset.sql);
    sqlDo(dataset, socket)
}

module.exports.getScanners = (socket) => {
    let dataset = {};
    console.log('try to get scanners');
    dataset.sql = "SELECT \n" +
        " raspberries.id\n" +
        ", raspberries.mac\n" +
        ", raspberries.name\n" +
        ", raspberries.timeout\n" +
        ", raspberries.createAt\n" +
        ", regions.name as regionName\n" +
        ", areas.name as areaName\n" +
        ", locations.name as locationName\n" +
        ", regions.id as regionSelfId\n" +
        ", areas.id as areaSelfId\n" +
        ", locations.id as locationSelfId\n" +
        "\n" +
        "FROM raspberries\n" +
        "LEFT JOIN locations ON\n" +
        "(raspberries.locationId = locations.id)\n" +
        "LEFT JOIN areas ON\n" +
        "(locations.areaId = areas.id)\n" +
        "LEFT JOIN regions ON\n" +
        "(areas.regionId = regions.id)";
    dataset.emit = "scannersList";
    dataset.type = "scannersList";
    console.log(dataset.sql);
    sqlDo(dataset, socket);
};

module.exports.getRoundsForMachine = function (dataset, socket) {
    if ((typeof dataset.date === 'undefined') || (typeof dataset.shift === 'undefined') || (typeof dataset.locationId === 'undefined')) {
        console.log(dataset);
        console.log('error');
        return;
    }
    console.log(dataset);
    let timeStart = "";
    let timeEnd = "";
    let start = "";
    let end = "";
    switch (dataset.shift) {
        case 'day':
            start = new Date(dataset.date + ' 01:00:00.000Z');
            end = new Date(dataset.date + ' 13:00:00.000Z');
            break;
        case 'night':
            start = new Date(dataset.date + ' 13:00:00.000Z');
            console.log(start);
            end = new Date(dataset.date + ' 13:00:00.000Z');
            console.log(end);
            end.setHours(end.getHours() + 12);
            console.log(end);
            break;
        default:
            start = new Date(dataset.date + ' 01:00:00.000Z');
            end = new Date(dataset.date + ' 01:00:00.000Z');
            end.setHours(end.getHours() + 24);
            break;
    }

    let sql = "SELECT rounds.mac\n" +
        ", rounds.start\n" +
        ", rounds.end\n" +
        ", TIME_TO_SEC(rounds.end) - TIME_TO_SEC(rounds.start) as duration\n" +
        ", machines.name\n" +
        ", machines.number\n" +
        ", locations.name as location\n" +
        ", '" + dataset.date + "' as date\n" +
        ", '" + dataset.shift + "' as shift\n" +
        "FROM truks.rounds\n" +
        "LEFT JOIN truks.machines\n" +
        "ON (truks.machines.mac = truks.rounds.mac)\n" +
        "LEFT JOIN truks.raspberries\n" +
        "ON (truks.raspberries.id = truks.rounds.raspberryId)\n" +
        "LEFT JOIN truks.locations\n" +
        "ON (truks.locations.id = " + dataset.locationId.toString().replace(/\D/g) + ")\n" +
        "WHERE\n" +
        "rounds.mac in (\n'" +
        //"SELECT DISTINCT mac FROM machines\n" +
        dataset.mac +
        "')\n" +
        "AND rounds.raspberryId in\n" +
        "(\n" +
        "        SELECT raspberries.id FROM raspberries " +
        " WHERE raspberries.locationId = " + dataset.locationId.toString().replace(/\D/g) + "\n" +
        ")\n" +
        "AND start > \"" + dateformat(start, config.get('dateformat')) + "\" \n" +
        "AND end < \"" + dateformat(end, config.get('dateformat')) + "\" \n" +
        "AND TIME_TO_SEC(end) - TIME_TO_SEC(start) > raspberries.timeout";
    // + config.get('correct_interval') + "\n";
    console.log('sql:\n', sql);
    connection.query(sql, function (err, results, fields) {
        socket.emit('rounds', results);
        console.log(results);
    })
};

module.exports.getListLocations = function (socket) {
    console.log('get locations list');
    let sql = "SELECT * FROM locations";
    connection.query(sql, function (error, results, fields) {

        socket.emit('locations', results);


    })
};

module.exports.getRegions = function (socket) {
    let dataset = {};
    dataset.type = "regions";
    dataset.sql = "SELECT id as value, name FROM regions";
    sqlDo(dataset, socket)
}
module.exports.getAreas = function (dataset, socket) {
    if (!dataset.id) {
        return
        // socket.emit('error')
    }
    console.log('Areas');
    dataset.type = "areas";
    dataset.sql = "SELECT * FROM areas WHERE regionId = " + dataset.id.toString().replace(/\D/g, "");
    sqlDo(dataset, socket)
}
module.exports.getLocations = function (dataset, socket) {
    if (!dataset.id) {
        return
        // socket.emit('error')
    }
    console.log('Locations');
    dataset.type = "locations";
    dataset.sql = "SELECT * FROM locations WHERE areaId = " + dataset.id.toString().replace(/\D/g, "");
    sqlDo(dataset, socket)
}

module.exports.addItem = function (dataset, socket) {

    console.log(dataset);
    if (dataset.typeItem && dataset.item) {
        let sql = "";
        if (dataset.typeItem === "region") {
            sql = "INSERT INTO regions(name) VALUES ('" + dataset.item + "');";

        }
        else if (dataset.typeItem === "area") {
            sql = "INSERT INTO areas (name, regionId) VALUES ('" + dataset.item + "', " + dataset.region + ");";
        }
        else if (dataset.typeItem === "location") {
            sql = "INSERT INTO locations (name, areaId) VALUES ('" + dataset.item + "', " + dataset.area + ");";
        }
        else {
            sql = false
        }
        console.log(sql);
        if (sql) {
            dataset.sql = sql;
            dataset.type = "itemadded";
            sqlDo(dataset, socket);
        }

    }
}

module.exports.addNewScanner = function (dataset, socket) {
    if ((dataset.mac) && (dataset.location) && (dataset.name)) {
        let sql = "INSERT INTO `raspberries`\n" +
            "(" +
            "`mac`,\n" +
            "`locationId`,\n" +
            "`name`,\n" +
            "`timeout`\n" +
            ")\n" +
            "VALUES\n" +
            "(\n" +
            "'" + dataset.mac + "',\n" +
            "" + dataset.location + ",\n" +
            "'" + dataset.name + "',\n" +
            "'" + dataset.timeout + "'\n" +
            ");\n"
        dataset.type = "raspberry";
        dataset.sql = sql;
        console.log(sql);
        sqlDo(dataset, socket);
    }
    else {
        return
    }
}

module.exports.getCandidates = function (socket) {
    let sql = "SELECT * FROM candidates";
    let dataset = {};
    dataset.type = "candidates";
    dataset.sql = sql;
    sqlDo(dataset, socket)
}

function sqlDo(dataset, socket) {
    let emit = dataset.emit || 'data';
    connection.query(dataset.sql, function (err, results, fields) {
        if (err) console.log(err);
        dataset.array = results;
        socket.emit(emit, dataset);
    })
}