let fn = require('./fn.js');
let config = require('./config');

module.exports.paths = function (socket) {
    console.log('a user connected');
    // fn.getStatisticsOfAnswers(socket);
    fn.getListLocations(socket);
    socket.on('getRounds', function (dataset) {
        fn.getRounds(dataset,socket);
    });
    socket.on('getRoundsForMachine', function (dataset) {
        fn.getRoundsForMachine(dataset,socket);
    });
    socket.on('getLocationsList', function () {
        fn.getListLocations(socket);
    })
    socket.on('lastseen', function () {
        fn.lastSeen(socket);
    })
    socket.on('lastseenScanner', function () {
        fn.lastSeenScanners(socket);
    })
    socket.on('data', function () {
        fn.getRegions(socket);
    })
}