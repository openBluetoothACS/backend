let socket = io(document.domain + ":32453");

function addZero(digits_length, source) {
    "use strict"
    let text = source + '';
    while (text.length < digits_length)
        text = '0' + text;
    return text;
}

let date = new Date();
let dateStr = date.getFullYear() + "-" + addZero(2, (date.getMonth() + 1)) + "-" + addZero(2, date.getDate());


socket.on('connect', function () {
    console.log('connection establish');
    socket.emit('getLocationsList');
});
socket.on('fullState', function (dataset) {
    let tmpArr = main.machines.concat(dataset);
    main.machines = tmpArr;
});
socket.on('locations', function (dataset) {
    main.locations = dataset;
});


let main = new Vue({
    el: '#main',
    data: {
        machines: [],
        locations: [],
        location: "",
        shift: "",
        date: dateStr
    },
    methods:
        {
            getRounds: function () {
                console.log('hello');
                let dataset = {
                    date: this.date,
                    locationId: this.location,
                    shift: this.shift
                };
                socket.emit('getRounds', dataset);
            },
            clearMachines: function () {
                this.machines = [];
            }
        }

});